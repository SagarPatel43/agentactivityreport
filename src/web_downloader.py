"""
Controls the browser instance in order to execute the web requests to download all the necessary agent reports
"""

from threading import Thread
import time
import sys

import Test
import browser


def download_reports(test):
    """
    Downloads, renames and stores all reports requested

    :param test: test object to run chromedriver
    """
    try:
        test.logging_in()
        test.swap_to_reporting()
        time.sleep(4)
        test.choose_template()
        test.choose_format()
        test.change_start_time()
        test.change_end_time()
        test.choose_call_completion()
        test.choose_short_duration()
        test.choose_sampling_type()
        test.generate_all_reports()
        test.set_template_name("Agent Activity Report")
        test.browser_driver.initiate_new_download_directory("Agent Activity Report")
        test.choose_template()
        test.choose_call_completion()
        test.choose_short_duration()
        test.generate_all_reports()
        test.set_template_name("Agent Call Detail Report")
        test.browser_driver.initiate_new_download_directory("Agent Call Detail Report")
        time.sleep(2)
        test.choose_template()
        time.sleep(2)
        test.generate_all_reports()
    except Exception as e:
        print(e)
    finally:
        if test is not None:
            test.browser_driver.quit_browser()
            test = None
    return


def get_reports(start_date, end_date):
    """
    Master function which will download all the reports

    Will Timeout after 20 minutes

    :param start_date: requested start date
    :param end_date:  requested end date
    """
    test = None
    thread = None
    try:
        browser_driver = browser.BrowserDriver()
        browser_driver.initialize()
        test = Test.TestMethods(browser_driver)
        test.set_start_date(start_date)
        test.set_end_date(end_date)

        thread = Thread(target=download_reports, args=[test])
        thread.start()
        thread.join(120)

        print('Timeout Exceeded')
        if thread.is_alive():
            raise Exception("Timeout Exceeded. Script took too long. Please try again")
    except Exception as e:
        print(e)
        sys.exit(1)
    finally:
        # Free up resources
        if test is not None:
            test.browser_driver.quit_browser()
            test = None

        thread.join()
