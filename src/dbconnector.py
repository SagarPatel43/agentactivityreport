"""
Connects to flowable db from BPMN Web Portal
"""

import jaydebeapi
import os


def get_agents(id):
    """
    Gets a list of agents to use in data calculations

    :param id: id to retrieve the list of agents from the db
    :return: list of agents to use
    """
    conn = jaydebeapi.connect("org.h2.Driver", "jdbc:h2:file:./flowable;AUTO_SERVER=TRUE;",
                              ["flowable", "flowable"],
                              "src/main/resources/jar/h2-1.4.199.jar")
    curs = conn.cursor()


    curs.execute(
        "SELECT AGENT_NAME FROM SCHEDULE_CONFIGURATION.NAME WHERE CONFIG_ID = " + str(id) + ";")

    agents = curs.fetchall()
    agents = [str(agent)[2:-3].lower() for agent in agents]
    curs.close()
    conn.close()
    curs = None
    conn = None
    return agents
