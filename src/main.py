"""
    Main file that drives program execution
"""

import datetime
import sys

import compute
import constants
import dbconnector as db
import report_parsing_service
import web_downloader
import write_total_report


def verify_args():
    """
    **Checks Command Line Arguments**

    This method checks the command line arguments.

    :return: start and end date formatted and the process id

    There should be 3 args in the form::

        main.py <startDate (yyyy-mm-dd)> <endDate (yyyy-mm-dd)> <process_id>

    """

    if len(sys.argv) != 4:
        print("Usage: main.py <startDate (yyyy-mm-dd)> <endDate (yyyy-mm-dd)> <process_id>")
        sys.exit(1)

    try:
        start_date = datetime.datetime.strptime(sys.argv[1], constants.ARG_FORMAT)
        end_date = datetime.datetime.strptime(sys.argv[2], constants.ARG_FORMAT)
    except Exception as e:
        print("Invalid date format. Format should be yyyy-mm-dd")
        sys.exit(1)

    if start_date >= end_date:
        print("Start date must be less than end date.")
        sys.exit(1)

    return start_date.strftime(constants.FILE_DATE_FORMAT), end_date.strftime(constants.FILE_DATE_FORMAT), sys.argv[3]


def main():
    """
    **Runs the Application**

    This method checks the command line arguments, gets the agent names, downloads data, parses data and outputs to a final excel document.
    """

    # get cli args
    start_date, end_date, id = verify_args()

    # check if list_of_names file has been provided
    list_of_names = db.get_agents(id)

    # download reports
    web_downloader.get_reports(start_date, end_date)

    # read data
    dictionary = report_parsing_service.read_all_files(list_of_names)

    # compute data
    computed_data = compute.compute_all_data(dictionary)

    # write data
    write_total_report.write_save_total_report(computed_data, start_date, end_date)

    return


if __name__ == "__main__":
    main()

