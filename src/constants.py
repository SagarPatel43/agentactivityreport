"""
Constants used throughout application
"""

# Background Color
WHITE_DECIMAL = 16777215.0

# Date Constants
DATE_FORMAT = "%m/%d/%Y, %I:%M:%S %p"
ARG_FORMAT = '%Y-%m-%d'
FILE_DATE_FORMAT = '%m/%d/%Y'

# Data Folders
AGENT_CDR_FOLDER = "AgentCallDetailReport"
AGENT_AR_FOLDER = "AgentActivityReport"
AGENT_UR_FOLDER = "AgentUnavailabilityReport"
ATTACHMENTS = "attachments"
XLS = ".xls"

# 3 data sets
AGENT_CDR = "Agent_CDR"
AGENT_AR = "Agent_AR"
AGENT_UR = "Agent_UR"

AGENT_CDR_TABLE_NUMBER = 1
AGENT_AR_UR_TABLE_NUMBER = 3

# final key values
NINEONEONE = "911"
CUSTOMER_CARE = "Customer Care"

AGENT = 'Agent'
AGENT_NAME = "Agent Name"
NAME = "Name"

INBOUND_BROADWORKS = "Inbound Broadworks"
EXTENSION = "Extension In&Out"
OUTBOUND_CALL = "Outbound Calls"
BROADWORKSAR = "Broadworks Activity Report"

CALLS = "Calls"
AHT = "AHT"
HOLD = "HOLD"
TIME = "Time"
TRANSFER = "Trans."
STAFFED = "Staffed"
AVAIL = "AVA"
UNAVAIL = "UNAVA"
BREAK = "BREAK"
TRN = "TRN"
TALK = "Talk"
OCCUPANCY = "Occup%"
OCCUP_HEADER = 'Occup\n%'
HOLD_HEADER = "Hold"
TOTAL = "Total"
AVG = "Average"
TOTAL_AVG = "Total_Avg"
AVERAGE = "Avg"
SUMMARY = "Summary"

# key data
CALL_TYPE = "Call Type"
CALL_START_TIME = "Call Start Time"
CALL_END_TIME = "Call End Time"
TALK_TIME = "Talk Time"
HOLD_TIME = "Hold Time"
WRAP_UP_TIME = "Wrap Up Time"
TRANSFER_NUMBER = "Transfer Number"
BREAK = "BREAK"
MEETING = "MEETING"
UNAVAILABLE = "Unavailable"
WAIT_TIME_IN_QUEUE = "Wait Time In Queue"

# data values
INBOUND_ACD = "Inbound ACD"
INTERNAL = "Internal"
OUTBOUND_ACD = "Outbound ACD"
OUTBOUND = "Outbound"

# Monthly Report Name
MONTHLY_REPORT = 'Agent Activity Report_'
XLSX = ".xlsx"

INVALID = "INVALID"

NONE = "None"

# dictionary per agent
AGENT_DICT = {
    INBOUND_BROADWORKS: {CALLS: "", AHT: "", HOLD: "", TIME: "", TRANSFER: ""},
    EXTENSION: {CALLS: "", AHT: ""},
    OUTBOUND_CALL: {CALLS: "", AHT: ""},
    BROADWORKSAR: {INVALID: False, STAFFED: "", AVAIL: "", UNAVAIL: "", BREAK: "", TRN: "", TALK: ""},
    OCCUPANCY: ""}

TABLE_HEADERS = {
    INBOUND_BROADWORKS: {CALLS: "", AHT: "", HOLD_HEADER: "", TIME: "", TRANSFER: ""},
    EXTENSION: {CALLS: "", AHT: ""},
    OUTBOUND_CALL: {CALLS: "", AHT: ""},
    BROADWORKSAR: {STAFFED: "", AVAIL: "", UNAVAIL: "", BREAK: "", TRN: "", TALK: ""}}

TABLE_HEADERS_COMPLETE = {
    INBOUND_BROADWORKS: {CALLS: "", AHT: "", HOLD: "", TIME: "", TRANSFER: ""},
    EXTENSION: {CALLS: "", AHT: ""},
    OUTBOUND_CALL: {CALLS: "", AHT: ""},
    BROADWORKSAR: {STAFFED: "", AVAIL: "", UNAVAIL: "", BREAK: "", TRN: "", TALK: ""},
    OCCUPANCY: ""}

# dictionary for total and averages
TABLE_TOTAL = {STAFFED: {TOTAL: 0, AVG: 0},
               AVAIL: {TOTAL: 0, AVG: 0},
               UNAVAIL: {TOTAL: 0, AVG: 0},
               BREAK: {TOTAL: 0, AVG: 0},
               TRN: {TOTAL: 0, AVG: 0},
               TALK: {TOTAL: 0, AVG: 0},
               OCCUPANCY: {AVG: 0, TOTAL: ""}
               }


DURATION_HEADERS = [WAIT_TIME_IN_QUEUE, TALK_TIME, HOLD_TIME, WRAP_UP_TIME]

BROADWORKSAR_ROW_TABLE = {STAFFED: [], AVAIL: [], UNAVAIL: [], BREAK: [],
                             TRN: [], TALK: [], OCCUPANCY: []}
