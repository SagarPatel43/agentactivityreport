"""
Processes the calculated data and writes to an excel document
"""

import os
import shutil
from datetime import datetime
from pathlib import Path

import cell_styles
import constants
import openpyxl
from openpyxl.utils import get_column_letter


# Adds headers at the top of the report
def __write_headers(sheet):
    # Manually add first header... Agent
    # Have to provide cell coordinate, cell value, cell style, and merge cells is used to span more cells
    cell = sheet.cell(row=1, column=1)
    cell.value = constants.AGENT
    sheet.merge_cells(start_row=1, end_row=2, start_column=1, end_column=1)
    cell.style = cell_styles.LARGE_HEADER_STYLE

    # Columns after 'Agent'
    # Table headers are hardcoded in constants
    column_number = 2
    for table in constants.TABLE_HEADERS:
        # Add wide grey headers
        cell = sheet.cell(row=1, column=column_number)
        cell.value = table
        cell.style = cell_styles.WIDE_HEADER_STYLE
        # Find cells to span using dictionary keys
        end_col = column_number + len(constants.TABLE_HEADERS[table]) - 1
        sheet.merge_cells(start_row=1, end_row=1, start_column=column_number, end_column=end_col)

        # Add subheadings under grey header
        for key in constants.TABLE_HEADERS[table]:
            # INVALID is not part of the table headers, it is only for information purposes
            if key != constants.INVALID:
                cell = sheet.cell(row=2, column=column_number)
                cell.value = key
                cell.style = cell_styles.LARGE_HEADER_STYLE
                column_number += 1

    # Manually add last header... Occup%
    cell = sheet.cell(row=1, column=column_number)
    cell.value = constants.OCCUP_HEADER
    sheet.merge_cells(start_row=1, end_row=2, start_column=column_number, end_column=column_number)
    cell.style = cell_styles.LARGE_HEADER_STYLE

    return column_number


def __set_cell_style(cell, total_dict, agent_key, grouping_key):
    # Empty cells are greyed out
    if cell.value == "":
        cell.style = cell_styles.GREY_STYLE
    # INVALID key is used to specify when data is 'corrupted', these are painted red
    elif grouping_key == constants.OCCUPANCY and total_dict[agent_key][constants.BROADWORKSAR][constants.INVALID] or \
            grouping_key == constants.BROADWORKSAR and total_dict[agent_key][grouping_key][constants.INVALID]:
        cell.style = cell_styles.INVALID_STYLE
    # Default cell style
    else:
        cell.style = cell_styles.CELL_STYLE


# Adds all data with cell style
def __add_data(sheet, total_dict):
    # Below headers, beginning of data
    row_count = 3
    col_count = 1

    # Add Agent Name (generator is used to avoid TOTAL_AVG Key when reading agent names)
    agent_key_generator = (key for key in sorted(total_dict) if key != constants.TOTAL_AVG)
    for agent_key in agent_key_generator:
        cell = sheet.cell(row=row_count, column=col_count)
        cell.value = agent_key
        cell.style = cell_styles.LEFT_JUST_STYLE
        col_count += 1

        # For each grey header
        for grouping_key in constants.TABLE_HEADERS_COMPLETE:
            # Occupancy is unique since it does not contain subheadings
            if grouping_key == constants.OCCUPANCY:
                cell = sheet.cell(row=row_count, column=col_count)
                cell.value = total_dict[agent_key][grouping_key]
                __set_cell_style(cell, total_dict, agent_key, grouping_key)

            # Go through each sub heading, cell style is set depending on value in dict
            for value in constants.TABLE_HEADERS_COMPLETE[grouping_key]:
                cell = sheet.cell(row=row_count, column=col_count)
                cell.value = total_dict[agent_key][grouping_key][value]
                __set_cell_style(cell, total_dict, agent_key, grouping_key)
                col_count += 1
        row_count += 1
        col_count = 1

    return row_count


# Formats cells after they have been placed with their corresponding style
def __format_cells(sheet, ending_row, ending_column):
    # Freeze agent name + table headers (hardcoded)
    sheet.freeze_panes = "B3"

    # Goes through every row in each column to determine cell with max length, then adjusts the columns width accordingly
    # (Essentially Excel's autofit)
    for col in range(1, ending_column):
        max_length = 0
        for row in range(2, ending_row):
            cell = sheet.cell(row=row, column=col)
            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except:
                pass

        if col == 1:
            sheet.column_dimensions[get_column_letter(col)].width = max_length
        else:
            sheet.column_dimensions[get_column_letter(col)].width = (max_length + 5) * 1.2

    # Find first subheading of each grouping and sets the border thickness (solely for looks)
    col_count = 2
    for row in range(2, ending_row):
        for key in constants.TABLE_HEADERS_COMPLETE:
            # Occupancy is the last header so it requires LR border to complete table
            if key == constants.OCCUPANCY:
                sheet.cell(row=row, column=col_count).border = cell_styles.THICK_LR_BORDER
            # Every leftmost subheader of each grey header is given a thick left border
            else:
                sheet.cell(row=row, column=col_count).border = cell_styles.THICK_LEFT_BORDER
            # Iterate by number of sub headings in each heading (to reach leftmost subheader)
            col_count += len(constants.TABLE_HEADERS_COMPLETE[key])
        col_count = 2


# Adds total and average at the bottom of the report
def __write_total_avg(sheet, total_dict, ending_row, ending_column):
    # Add row headers, spacing is fixed
    total_cell_row = ending_row + 2
    average_cell_row = total_cell_row + 1

    total_cell = sheet.cell(row=total_cell_row, column=1)
    total_cell.value = constants.TOTAL
    total_cell.style = cell_styles.ROW_HEADER_STYLE

    average_cell = sheet.cell(row=average_cell_row, column=1)
    average_cell.value = constants.AVERAGE
    average_cell.style = cell_styles.ROW_HEADER_STYLE

    # Merge cells until last grey header where total/avg will be written
    end_merge_column = ending_column - len(constants.TABLE_HEADERS_COMPLETE[constants.BROADWORKSAR]) - 1

    sheet.merge_cells(start_row=total_cell_row, end_row=total_cell_row, start_column=1, end_column=end_merge_column)
    sheet.merge_cells(start_row=average_cell_row, end_row=average_cell_row, start_column=1, end_column=end_merge_column)

    # Add data
    col_count = end_merge_column + 1
    for table_key in constants.TABLE_TOTAL:
        total_cell = sheet.cell(row=total_cell_row, column=col_count)
        average_cell = sheet.cell(row=average_cell_row, column=col_count)

        total_cell.value = total_dict[constants.TOTAL_AVG][table_key][constants.TOTAL]
        average_cell.value = total_dict[constants.TOTAL_AVG][table_key][constants.AVG]

        average_cell.style = cell_styles.CELL_STYLE

        # This is a hardcoded style
        if table_key == constants.OCCUPANCY:
            total_cell.style = cell_styles.GREY_STYLE
        else:
            total_cell.style = cell_styles.CELL_STYLE

        col_count += 1

    # Setting cell style for empty rows (just for visuals)
    for row in range(ending_row, total_cell_row):
        for col in range(1, ending_column+1):
            cell = sheet.cell(row=row, column=col)
            if col == 1:
                cell.style = cell_styles.LEFT_JUST_STYLE
            else:
                cell.style = cell_styles.CELL_STYLE

    return average_cell_row + 1


def write_report(sheet, data):
    """
    Writes to the excel sheet
    :param sheet: sheet to write to
    :param data: data to write
    """

    ending_column = __write_headers(sheet)
    ending_row = __add_data(sheet, data)
    ending_row_updated = __write_total_avg(sheet, data, ending_row, ending_column)
    __format_cells(sheet, ending_row_updated, ending_column)


def delete_temp_directories():
    """
    Deletes all the downloaded data
    """
    directory_names = [constants.AGENT_CDR_FOLDER, constants.AGENT_AR_FOLDER, constants.AGENT_UR_FOLDER]
    for name in directory_names:
        directory_path = os.path.abspath(name)
        if os.path.exists(directory_path):
            shutil.rmtree(directory_path)


def get_wb_name(start_date, end_date):
    """
    Gets the corresponding work book name based on the current date iteration
    :param start_date: start date requested
    :param end_date: end date requested
    :return: the wb name based on start and end date
    """
    wb_name = constants.MONTHLY_REPORT
    start_time = datetime.strptime(start_date, constants.FILE_DATE_FORMAT)
    end_time = datetime.strptime(end_date, constants.FILE_DATE_FORMAT)

    if end_time.month - start_time.month == 1:
        wb_name += start_time.strftime('%B%Y')
    else:
        wb_name += start_time.strftime('%m%d%Y')

    wb_name += constants.XLSX

    return wb_name


def write_save_total_report(total_dict, start_date, end_date):
    """
    Writes all the data calculated into an excel document with a total worksheet and a worksheet for each day

    :param total_dict: the calculated data
    :param start_date: start date requested
    :param end_date: end date requested
    """
    wb = openpyxl.Workbook()
    sheet = wb.active
    sheet.title = constants.TOTAL

    write_report(sheet, total_dict[constants.TOTAL])

    del total_dict[constants.TOTAL]

    for date, data in sorted(total_dict.items(), key=lambda x: int(x[0])):
        # formats date ex: 050102019 -> 05-01-19
        formatted_date = "".join(date[i:i + 2] + ("-" if i != 6 else "") for i in {0, 2, 6})
        sheet = wb.create_sheet(formatted_date)

        write_report(sheet, total_dict[date])

    delete_temp_directories()

    working_directory = Path(os.getcwd())
    if os.path.exists(working_directory / constants.ATTACHMENTS):
        shutil.rmtree(working_directory / constants.ATTACHMENTS)
    os.makedirs(working_directory / constants.ATTACHMENTS)

    wb.save(working_directory / constants.ATTACHMENTS / get_wb_name(start_date, end_date))
