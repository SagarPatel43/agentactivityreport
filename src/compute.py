"""
Contains methods used to process the downloaded data into the total data which can then be added to the final report document
"""

from win32com.client import constants
import constants
from copy import deepcopy
from datetime import datetime
from statistics import mean
from collections import Counter


def __convert_seconds_to_time(time_sec):
    """
    Function to convert seconds to hour minute seconds string

    :param time_sec: time in seconds
    :type: int
    :return: the time in HH:mm:ss format
    """
    if time_sec < 0:
        return time_sec

    hours = time_sec // 3600

    minutes = (time_sec % 3600) // 60

    seconds = time_sec % 60

    return "%02i:%02i:%02i" % (hours, minutes, seconds)


# compute all data for Inbound Broadworks
def __compute_inbound_call_and_aht(dictionary, final_dict, total_daily_time):
    """
    Computes data for the call and aht time for inbound calls

    :param dictionary: dictionary containing downloaded data
    :param final_dict: dictionary to be written to with calculated data
    :param total_daily_time: dictionary with total data for all calls
    """
    for agent in dictionary[constants.AGENT_CDR]:
        # Data to calculate
        total_calls = 0
        total_time = []
        total_hold = 0
        total_hold_time = []
        count_transfers = 0

        # initialize dictionary
        total_daily_time.setdefault(agent, {})

        for date in dictionary[constants.AGENT_CDR][agent]:
            daily_calls = 0
            daily_time = []
            daily_hold = 0
            daily_hold_time = []
            daily_transfers = 0
            for agent_data in dictionary[constants.AGENT_CDR][agent][date]:

                # If call is inbound acd, use the data
                if agent_data[constants.CALL_TYPE] in constants.INBOUND_ACD:
                    daily_calls += 1
                    daily_time.append(agent_data[constants.TALK_TIME] + agent_data[constants.HOLD_TIME] + agent_data[
                        constants.WRAP_UP_TIME])

                    if int(agent_data[constants.HOLD_TIME]) > 0:
                        daily_hold += 1
                        daily_hold_time.append(int(agent_data[constants.HOLD_TIME]))

                    if agent_data[constants.TRANSFER_NUMBER] != constants.NONE:
                        daily_transfers += 1

            total_calls += daily_calls
            total_hold += daily_hold
            count_transfers += daily_transfers

            # initialize dictionary
            total_daily_time[agent].setdefault(date, [])

            # initialize total dictionary
            final_dict.setdefault(date, {})
            final_dict[date].setdefault(agent, deepcopy(constants.AGENT_DICT))
            final_dict[date][agent][constants.INBOUND_BROADWORKS][constants.CALLS] = daily_calls
            final_dict[date][agent][constants.INBOUND_BROADWORKS][constants.HOLD] = daily_hold
            final_dict[date][agent][constants.INBOUND_BROADWORKS][constants.TRANSFER] = daily_transfers

            # compute daily values and store in daily dictionary
            if len(daily_time) > 0:
                total_time.append(mean(daily_time))
                total_daily_time[agent][date].append(sum(daily_time))
                final_dict[date][agent][constants.INBOUND_BROADWORKS][constants.AHT] = __convert_seconds_to_time(
                    mean(daily_time))
            else:
                final_dict[date][agent][constants.INBOUND_BROADWORKS][constants.AHT] = "DIV/0"

            if len(daily_hold_time) > 0:
                total_hold_time.append(mean(daily_hold_time))
                final_dict[date][agent][constants.INBOUND_BROADWORKS][constants.TIME] = __convert_seconds_to_time(
                    mean(daily_hold_time))
            else:
                final_dict[date][agent][constants.INBOUND_BROADWORKS][constants.TIME] = "DIV/0"

        # store all data in total dictionary
        final_dict.setdefault(constants.TOTAL, {})
        final_dict[constants.TOTAL].setdefault(agent, deepcopy(constants.AGENT_DICT))
        final_dict[constants.TOTAL][agent][constants.INBOUND_BROADWORKS][constants.CALLS] = total_calls
        final_dict[constants.TOTAL][agent][constants.INBOUND_BROADWORKS][constants.HOLD] = total_hold
        if len(total_time) > 0:
            final_dict[constants.TOTAL][agent][constants.INBOUND_BROADWORKS][constants.AHT] = __convert_seconds_to_time(mean(total_time))
        else:
            final_dict[constants.TOTAL][agent][constants.INBOUND_BROADWORKS][constants.AHT] = "DIV/0"

        if len(total_hold_time) > 0:
            final_dict[constants.TOTAL][agent][constants.INBOUND_BROADWORKS][constants.TIME] = __convert_seconds_to_time(mean(total_hold_time))
        else:
            final_dict[constants.TOTAL][agent][constants.INBOUND_BROADWORKS][constants.TIME] = "DIV/0"

        final_dict[constants.TOTAL][agent][constants.INBOUND_BROADWORKS][constants.TRANSFER] = count_transfers
    return


# compute all data for Extension In & Out
def __compute_ext_call_and_aht(dictionary, final_dict, total_daily_time):
    """
    Computes data for the call and aht time for ext calls

    :param dictionary: dictionary containing downloaded data
    :param final_dict: dictionary to be written to with calculated data
    :param total_daily_time: dictionary with total data for all calls
    """

    for agent in dictionary[constants.AGENT_CDR]:
        # Data to calculate
        total_calls = 0
        total_time = []

        # initialize dictionary
        total_daily_time.setdefault(agent, {})

        for dateNum in dictionary[constants.AGENT_CDR][agent]:
            prev_end_time = datetime.min
            daily_calls = 0
            daily_time = []
            for agent_data in dictionary[constants.AGENT_CDR][agent][dateNum]:

                # determine if call is on hold
                date = datetime.strptime(str(agent_data[constants.CALL_START_TIME]), '%Y-%m-%d %H:%M:%S')
                on_hold = date < prev_end_time and agent_data[constants.CALL_TYPE] == constants.INTERNAL

                # If call is internal and not on hold and no transfer number, use the data
                if agent_data[constants.CALL_TYPE] in constants.INTERNAL and (not on_hold) and agent_data[
                    constants.TRANSFER_NUMBER] == constants.NONE:

                    daily_calls += 1
                    daily_time.append(agent_data[constants.TALK_TIME] + agent_data[constants.HOLD_TIME] + agent_data[
                        constants.WRAP_UP_TIME])

                prev_end_time = datetime.strptime(str(agent_data[constants.CALL_END_TIME]), '%Y-%m-%d %H:%M:%S')

            total_calls += daily_calls

            # initialize dictionary
            total_daily_time[agent].setdefault(dateNum, [])

            # initialize total dictionary
            final_dict.setdefault(dateNum, {})
            final_dict[dateNum].setdefault(agent, deepcopy(constants.AGENT_DICT))
            final_dict[dateNum][agent][constants.EXTENSION][constants.CALLS] = daily_calls

            # compute daily values and store in daily dictionary
            if len(daily_time) > 0:
                total_time.append(mean(daily_time))
                total_daily_time[agent][dateNum].append(sum(daily_time))
                final_dict[dateNum][agent][constants.EXTENSION][constants.AHT] = __convert_seconds_to_time(
                    mean(daily_time))
            else:
                final_dict[dateNum][agent][constants.EXTENSION][constants.AHT] = "DIV/0"

        # store all data in total dictionary
        final_dict.setdefault(constants.TOTAL, {})
        final_dict[constants.TOTAL].setdefault(agent, deepcopy(constants.AGENT_DICT))
        final_dict[constants.TOTAL][agent][constants.EXTENSION][constants.CALLS] = total_calls
        if len(total_time) > 0:
            final_dict[constants.TOTAL][agent][constants.EXTENSION][constants.AHT] = __convert_seconds_to_time(mean(total_time))
        else:
            final_dict[constants.TOTAL][agent][constants.EXTENSION][constants.AHT] = "DIV/0"

    return


# compute all data for Outbound calls
def __compute_outbound_call_and_aht(dictionary, final_dict, total_daily_time):
    """
    Computes data for the call and aht time for outbound calls

    :param dictionary: dictionary containing downloaded data
    :param final_dict: dictionary to be written to with calculated data
    :param total_daily_time: dictionary with total data for all calls
    """

    for agent in dictionary[constants.AGENT_CDR]:
        # Data to calculate
        total_calls = 0
        total_time = []

        # initialize dictionary
        total_daily_time.setdefault(agent, {})

        for dateNum in dictionary[constants.AGENT_CDR][agent]:
            prev_end_time = datetime.min
            daily_calls = 0
            daily_time = []
            for agent_data in dictionary[constants.AGENT_CDR][agent][dateNum]:

                # determine if call is on hold
                date = datetime.strptime(str(agent_data[constants.CALL_START_TIME]), '%Y-%m-%d %H:%M:%S')
                on_hold = date < prev_end_time and agent_data[constants.CALL_TYPE] == constants.OUTBOUND

                # If call is outbound/outbound acd and not on hold, use the data
                if agent_data[constants.CALL_TYPE] in {constants.OUTBOUND_ACD, constants.OUTBOUND} and (not on_hold):
                    daily_calls += 1
                    daily_time.append(agent_data[constants.TALK_TIME] + agent_data[constants.HOLD_TIME] + agent_data[
                        constants.WRAP_UP_TIME])

                prev_end_time = datetime.strptime(str(agent_data[constants.CALL_END_TIME]), '%Y-%m-%d %H:%M:%S')

            total_calls += daily_calls

            # initialize dictionary
            total_daily_time[agent].setdefault(dateNum, [])

            # initialize total dictionary
            final_dict.setdefault(dateNum, {})
            final_dict[dateNum].setdefault(agent, deepcopy(constants.AGENT_DICT))
            final_dict[dateNum][agent][constants.OUTBOUND_CALL][constants.CALLS] = daily_calls

            # compute daily values and store in daily dictionary
            if len(daily_time) > 0:
                total_time.append(mean(daily_time))
                total_daily_time[agent][dateNum].append(sum(daily_time))
                final_dict[dateNum][agent][constants.OUTBOUND_CALL][constants.AHT] = __convert_seconds_to_time(
                    mean(daily_time))
            else:
                final_dict[dateNum][agent][constants.OUTBOUND_CALL][constants.AHT] = "DIV/0"

        # store all data in total dictionary
        final_dict.setdefault(constants.TOTAL, {})
        final_dict[constants.TOTAL].setdefault(agent, deepcopy(constants.AGENT_DICT))
        final_dict[constants.TOTAL][agent][constants.OUTBOUND_CALL][constants.CALLS] = total_calls
        if len(total_time) > 0:
            final_dict[constants.TOTAL][agent][constants.OUTBOUND_CALL][constants.AHT] = __convert_seconds_to_time(mean(total_time))
        else:
            final_dict[constants.TOTAL][agent][constants.OUTBOUND_CALL][constants.AHT] = "DIV/0"
    return


# compute all data for Broadworks Activity Report
def __compute_activity_data(dictionary, final_dict, total_daily_time):
    """
    Computes data for all activity data for agents

    :param dictionary: dictionary containing downloaded data
    :param final_dict: dictionary to be written to with calculated data
    :param total_daily_time: dictionary with total data for all calls
    """

    # list of values used for total sum/avg calculations
    total_staffed = []
    total_break_time = []
    total_trn = []
    total_unavail = []
    total_talk = []
    total_avail = []
    total_occupy = []

    # data per day for daily total sum/avg calculations
    daily_data = {}

    for agent in dictionary[constants.AGENT_UR]:

        # if agent doesnt have any previous data: skip
        if agent not in total_daily_time:
            continue

        # check if any data is invalid
        total_invalid = False

        # Data to calculate
        staffed_list = []
        break_time_list = []
        trn_list = []
        unavail_list = []
        talk_list = []
        avail_list = []
        occupy_list = []

        for date in dictionary[constants.AGENT_UR][agent]:
            # check if a certain day has invalid data
            daily_invalid = False

            # if date doesnt have any previous data: skip
            if date not in total_daily_time[agent]:
                continue

            # sum all data into one dictionary for a certain agent
            agent_data = Counter({})
            for elem in dictionary[constants.AGENT_UR][agent][date]:
                agent_data += Counter(elem)

            for elem in dictionary[constants.AGENT_AR][agent][date]:
                agent_data += Counter(elem)

            # data calculations
            staffed = agent_data[constants.STAFFED]
            break_time = agent_data[constants.BREAK]
            trn = agent_data[constants.MEETING]
            unavail = agent_data[constants.UNAVAILABLE] - (break_time + trn)
            talk = sum(total_daily_time[agent][date])
            avail = staffed - (unavail + talk + trn + break_time)

            # check if data is invalid
            if unavail < 0 or talk < 0 or avail < 0:
                total_invalid = True
                daily_invalid = True

            # update final dictionary for specific agent and date
            final_dict.setdefault(date, {})
            final_dict[date][agent][constants.BROADWORKSAR][constants.INVALID] = daily_invalid
            final_dict[date][agent][constants.BROADWORKSAR][constants.STAFFED] = __convert_seconds_to_time(staffed)
            final_dict[date][agent][constants.BROADWORKSAR][constants.BREAK] = __convert_seconds_to_time(break_time)
            final_dict[date][agent][constants.BROADWORKSAR][constants.TRN] = __convert_seconds_to_time(trn)
            final_dict[date][agent][constants.BROADWORKSAR][constants.UNAVAIL] = __convert_seconds_to_time(unavail)
            final_dict[date][agent][constants.BROADWORKSAR][constants.TALK] = __convert_seconds_to_time(talk)
            final_dict[date][agent][constants.BROADWORKSAR][constants.AVAIL] = __convert_seconds_to_time(avail)

            # add data for total calculations for total data
            staffed_list.append(staffed)
            break_time_list.append(break_time)
            trn_list.append(trn)
            unavail_list.append(unavail)
            talk_list.append(talk)
            avail_list.append(avail)

            # add data for total claculations for daily data
            daily_data.setdefault(date, deepcopy(constants.BROADWORKSAR_ROW_TABLE))
            daily_data[date][constants.STAFFED].append(staffed)
            daily_data[date][constants.BREAK].append(break_time)
            daily_data[date][constants.TRN].append(trn)
            daily_data[date][constants.UNAVAIL].append(unavail)
            daily_data[date][constants.TALK].append(talk)
            daily_data[date][constants.AVAIL].append(avail)

            try:
                # calculate occup data
                occupy = talk / (staffed - break_time - trn)

                # add to both daily and total dict and list
                final_dict[date][agent][constants.OCCUPANCY] = "{:.2%}".format(occupy)
                daily_data[date][constants.OCCUPANCY].append(occupy)

                occupy_list.append(occupy)
            except:
                pass

        # store all data in total dictionary
        final_dict[constants.TOTAL][agent][constants.BROADWORKSAR][constants.INVALID] = total_invalid
        final_dict[constants.TOTAL][agent][constants.BROADWORKSAR][constants.STAFFED] = __convert_seconds_to_time(sum(staffed_list))
        final_dict[constants.TOTAL][agent][constants.BROADWORKSAR][constants.BREAK] = __convert_seconds_to_time(sum(break_time_list))
        final_dict[constants.TOTAL][agent][constants.BROADWORKSAR][constants.TRN] = __convert_seconds_to_time(sum(trn_list))
        final_dict[constants.TOTAL][agent][constants.BROADWORKSAR][constants.UNAVAIL] = __convert_seconds_to_time(sum(unavail_list))
        final_dict[constants.TOTAL][agent][constants.BROADWORKSAR][constants.TALK] = __convert_seconds_to_time(sum(talk_list))
        final_dict[constants.TOTAL][agent][constants.BROADWORKSAR][constants.AVAIL] = __convert_seconds_to_time(sum(avail_list))

        # calculate sums per day and add to total
        total_staffed.append(sum(staffed_list))
        total_break_time.append(sum(break_time_list))
        total_trn.append(sum(trn_list))
        total_unavail.append(sum(unavail_list))
        total_talk.append(sum(talk_list))
        total_avail.append(sum(avail_list))

        # store and calculate occupy data
        if len(occupy_list) > 0:
            final_dict[constants.TOTAL][agent][constants.OCCUPANCY] = "{:.2%}".format(mean(occupy_list))
            total_occupy.append(mean(occupy_list))

    # iterate over each day
    for date in daily_data:

        # store daily data in final dictionary
        final_dict[date].setdefault(constants.TOTAL_AVG, deepcopy(constants.TABLE_TOTAL))

        final_dict[date][constants.TOTAL_AVG][constants.STAFFED][constants.TOTAL] = __convert_seconds_to_time(sum(daily_data[date][constants.STAFFED]))
        final_dict[date][constants.TOTAL_AVG][constants.STAFFED][constants.AVG] = __convert_seconds_to_time(mean(daily_data[date][constants.STAFFED]))

        final_dict[date][constants.TOTAL_AVG][constants.BREAK][constants.TOTAL] = __convert_seconds_to_time(sum(daily_data[date][constants.BREAK]))
        final_dict[date][constants.TOTAL_AVG][constants.BREAK][constants.AVG] = __convert_seconds_to_time(mean(daily_data[date][constants.BREAK]))

        final_dict[date][constants.TOTAL_AVG][constants.TRN][constants.TOTAL] = __convert_seconds_to_time(sum(daily_data[date][constants.TRN]))
        final_dict[date][constants.TOTAL_AVG][constants.TRN][constants.AVG] = __convert_seconds_to_time(mean(daily_data[date][constants.TRN]))

        final_dict[date][constants.TOTAL_AVG][constants.UNAVAIL][constants.TOTAL] = __convert_seconds_to_time(sum(daily_data[date][constants.UNAVAIL]))
        final_dict[date][constants.TOTAL_AVG][constants.UNAVAIL][constants.AVG] = __convert_seconds_to_time(mean(daily_data[date][constants.UNAVAIL]))

        final_dict[date][constants.TOTAL_AVG][constants.TALK][constants.TOTAL] = __convert_seconds_to_time(sum(daily_data[date][constants.TALK]))
        final_dict[date][constants.TOTAL_AVG][constants.TALK][constants.AVG] = __convert_seconds_to_time(mean(daily_data[date][constants.TALK]))

        final_dict[date][constants.TOTAL_AVG][constants.AVAIL][constants.TOTAL] = __convert_seconds_to_time(sum(daily_data[date][constants.AVAIL]))
        final_dict[date][constants.TOTAL_AVG][constants.AVAIL][constants.AVG] = __convert_seconds_to_time(mean(daily_data[date][constants.AVAIL]))

        final_dict[date][constants.TOTAL_AVG][constants.OCCUPANCY][constants.AVG] = "{:.2%}".format(mean(daily_data[date][constants.OCCUPANCY]))

    # compute total and averages per column
    final_dict.setdefault(constants.TOTAL, {})
    final_dict[constants.TOTAL].setdefault(constants.TOTAL_AVG, deepcopy(constants.TABLE_TOTAL))
    if len(total_staffed) > 0:
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.STAFFED][constants.TOTAL] = __convert_seconds_to_time(sum(total_staffed))
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.STAFFED][constants.AVG] = __convert_seconds_to_time(mean(total_staffed))

    if len(total_break_time) > 0:
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.BREAK][constants.TOTAL] = __convert_seconds_to_time(sum(total_break_time))
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.BREAK][constants.AVG] = __convert_seconds_to_time(mean(total_break_time))

    if len(total_trn) > 0:
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.TRN][constants.TOTAL] = __convert_seconds_to_time(sum(total_trn))
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.TRN][constants.AVG] = __convert_seconds_to_time(mean(total_trn))

    if len(total_unavail) > 0:
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.UNAVAIL][constants.TOTAL] = __convert_seconds_to_time(sum(total_unavail))
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.UNAVAIL][constants.AVG] = __convert_seconds_to_time(mean(total_unavail))

    if len(total_talk) > 0:
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.TALK][constants.TOTAL] = __convert_seconds_to_time(sum(total_talk))
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.TALK][constants.AVG] = __convert_seconds_to_time(mean(total_talk))

    if len(total_avail) > 0:
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.AVAIL][constants.TOTAL] = __convert_seconds_to_time(sum(total_avail))
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.AVAIL][constants.AVG] = __convert_seconds_to_time(mean(total_avail))

    if len(total_occupy) > 0:
        final_dict[constants.TOTAL][constants.TOTAL_AVG][constants.OCCUPANCY][constants.AVG] = "{:.2%}".format(mean(total_occupy))


def compute_all_data(data):
    """
    Init final dictionary and total daily time dictionary and populates with all the calculated data

    :param data: dictionary containing downloaded data
    :return: final dictionary with all computed data
    """

    # final dictionary for all data
    final_dict = {}

    # dictionary for daily time for each agent
    total_daily_time = {}

    # calculate all data
    __compute_inbound_call_and_aht(data, final_dict, total_daily_time)
    __compute_ext_call_and_aht(data, final_dict, total_daily_time)
    __compute_outbound_call_and_aht(data, final_dict, total_daily_time)
    __compute_activity_data(data, final_dict, total_daily_time)

    return final_dict
