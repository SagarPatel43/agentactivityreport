"""
Reads all the downloaded data and stores into a dictionary
"""

import os
import re
import sys
from collections import namedtuple
from copy import deepcopy
from datetime import datetime
from pathlib import Path

import win32com.client as win32

import constants

rowData = namedtuple("RowData", 'starting_row ending_row')
columnData = namedtuple("ColumnData", 'starting_column ending_column agent_name_column')


# Convert duration to seconds
def __get_duration_sec(time_decimal):
    if time_decimal is None:
        return 0
    else:
        return int(round(time_decimal * 24 * 3600))


# Dynamically determine the row where the table starts and ends
# Uses cell color change and text "summary" to determine table start/end
def __get_starting_ending_row(ws, table_number):
    table_starting_row = 1
    starting_row = 0
    ending_row = 0

    for table in range(table_starting_row, table_number+1):
        for i in range(table_starting_row, ws.Rows.Count):
            if ws.Cells(i, 1).Interior.Color != constants.WHITE_DECIMAL:
                starting_row = i
                break

        for i in range(starting_row, ws.Rows.Count):
            if ws.Cells(i, 1).Interior.Color == constants.WHITE_DECIMAL:
                ending_row = i
                table_starting_row = ending_row
                break
            elif ws.Cells(i, 2).Value == constants.SUMMARY:
                ending_row = i
                table_starting_row = ending_row + 1
                break

    return rowData(starting_row, ending_row)


# Checks for agent name header to determine start of columns for data to be read
# Uses cell color to determine when columns end
def __get_starting_ending_column(ws, row_data):
    starting_column = 0
    agent_name_column = 0
    ending_column = 0

    for j in range(1, ws.Columns.Count):
        if ws.Cells(row_data.starting_row, j).Value == constants.AGENT_NAME or ws.Cells(row_data.starting_row, j).Value == constants.NAME:
            agent_name_column = j
            starting_column = j + 1
            break

    for j in range(starting_column, ws.Columns.Count):
        if ws.Cells(row_data.starting_row, j).Interior.Color == constants.WHITE_DECIMAL:
            ending_column = j
            break

    return columnData(starting_column, ending_column, agent_name_column)


# Programmatically acquire table headers to be used in dictionary
# This could technically be hardcoded...
def get_table_columns(ws, row_data, column_data):
    """
    Gets the headers for each column in the table

    :param ws: excel worksheet to process
    :param row_data: data for each row
    :param column_data: data for each column
    :return: the table headers for each column
    """
    report_table = {}
    for table_col_index in range(column_data.starting_column, column_data.ending_column):
        report_table.setdefault(ws.Cells(row_data.starting_row, table_col_index).Value, None)

    return report_table


# Begin reading data
def parse_data(sheet, table_dict, table_dict_key, table_columns, day_number, row_data, column_data, list_of_names):
    """
    reads the data and populates the dictionary

    :param sheet: excel worksheet to process
    :param table_dict: total dictionary that contains the data read
    :param table_dict_key: key to store the data in the dictionary
    :param table_columns: column header data for the table
    :param day_number: the day of the table data
    :param row_data: the data for each row
    :param column_data: the data for each column
    :param list_of_names: list of agent names to use
    """
    for table_row in range(row_data.starting_row+1, row_data.ending_row):
        agent_name = sheet.Cells(table_row, column_data.agent_name_column).Value
        name_split = agent_name.split(', ')
        agent_name_formatted = name_split[1] + " " + name_split[0]
        # Only accept names that are in the list
        if agent_name_formatted.lower() not in list_of_names:
            continue

        # Initialize dictionary
        table_dict[table_dict_key].setdefault(agent_name, {})
        table_dict[table_dict_key][agent_name].setdefault(day_number, [])
        table_dict[table_dict_key][agent_name][day_number].append(deepcopy(table_columns))

        for col_index in range(column_data.starting_column, column_data.ending_column):
            # For each table column, go to newest dict in list (name -> day number -> list of dicts),
            # and put corresponding value into dictionary
            table_column_header = sheet.Cells(row_data.starting_row, col_index).Value
            table_column_value = sheet.Cells(table_row, col_index).Value

            # If this is data for Agent AR, Agent UR, or a set of headings in Agent CDR, convert value to
            # duration in seconds
            if table_dict_key == constants.AGENT_AR or table_dict_key == constants.AGENT_UR or \
                    table_column_header in constants.DURATION_HEADERS:

                table_dict[table_dict_key][agent_name][day_number][-1][table_column_header] = __get_duration_sec(table_column_value)

            # If this is data for below headers, convert value to datetime format
            elif table_column_header == constants.CALL_START_TIME or table_column_header == constants.CALL_END_TIME:

                table_dict[table_dict_key][agent_name][day_number][-1][table_column_header] = \
                    datetime.strptime(str(table_column_value), constants.DATE_FORMAT)

            # Else take string value
            else:
                table_dict[table_dict_key][agent_name][day_number][-1][table_column_header] = str(table_column_value)


def open_workbook(xlapp, xlfile):
    """
    Opens a excel file and the workbook

    :param xlapp: excel application instance
    :param xlfile: excel file path
    :return: excel workbook found in file
    """
    try:
        xlwb = xlapp.Workbooks(xlfile)
    except Exception as e:
        try:
            xlwb = xlapp.Workbooks.Open(xlfile)
        except Exception as e:
            print(e)
            xlwb = None
    return xlwb


def __parse_file_path(xlapp, table_dict, file_name, table_dict_key, table_number, list_of_names):
    file_path = Path(os.path.abspath(file_name))
    # Iterate through each file in the directory
    for file in os.listdir(file_path):
        # Only consider excel files
        if not file.endswith(constants.XLS):
            continue

        print("Now reading: " + file)
        try:
            xlwb = open_workbook(xlapp, file_path / file)
            sheet = xlwb.Worksheets(1)

            # Order matters here
            row_data = __get_starting_ending_row(sheet, table_number)
            column_data = __get_starting_ending_column(sheet, row_data)
            report_table = get_table_columns(sheet, row_data, column_data)

            # Extract day number from file name to be used for dictionary key
            day_number = re.search(r"\d+", file).group()
            parse_data(sheet, table_dict, table_dict_key, report_table, day_number, row_data, column_data, list_of_names)
            xlwb.Close()
        except Exception as e:
            print(e)
        finally:
            xlwb = None
            sheet = None


def read_all_files(list_of_names):
    """
    reads all files for processing and stores in a dictionary

    :param list_of_names: list of agents to be used
    :return: total dictionary with all data read
    """
    table_dict = {constants.AGENT_CDR: {}, constants.AGENT_AR: {}, constants.AGENT_UR: {}}

    # Table number refers to which table in the report we are concerned with (e.g. table at the top of report has 'x' data,
    # table at the bottom of report has 'y' data, if you want 'y' data specify table 2, otherwise table 1 (hardcoded in constants).
    xlapp = None
    try:
        xlapp = win32.gencache.EnsureDispatch('Excel.Application')

        __parse_file_path(xlapp, table_dict, constants.AGENT_CDR_FOLDER, constants.AGENT_CDR, constants.AGENT_CDR_TABLE_NUMBER, list_of_names)

        __parse_file_path(xlapp, table_dict, constants.AGENT_AR_FOLDER, constants.AGENT_AR, constants.AGENT_AR_UR_TABLE_NUMBER, list_of_names)

        __parse_file_path(xlapp, table_dict, constants.AGENT_UR_FOLDER, constants.AGENT_UR, constants.AGENT_AR_UR_TABLE_NUMBER, list_of_names)
    except Exception as e:
        print(e)
        sys.exit(1)
    finally:
        # RELEASES RESOURCES
        xlapp.Quit()
        xlapp = None

    return table_dict
