"""
Browser class which is used to start up an instance of google chrome for selenium to use
"""

import os
import shutil

from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class BrowserDriver:
    __browser_name = "chrome"
    __url_name = "https://broadfone2.iristel.ca/callcenter"
    __driver = None
    __current_directory = os.getcwd()
    __folder_name = "Agent Unavailability Report".replace(" ", "")
    __full_path = ""
    __path_to_folder = ""

    # Initialize the browser settings
    def initialize(self):
        """
        **Default Constructor for browser object**

        Initializes attributes for the browser and configures for use with selenium

        """
        if self.__browser_name == "chrome":
            chrome_options = Options()
            chrome_options.add_argument("--window-position=-32000,-32000")
            self.__path_to_folder = self.__current_directory + "\\" + self.__folder_name
            self.__full_path = self.__current_directory + "\\" + self.__folder_name + "\\reports"
            if os.path.exists(self.__path_to_folder):
                shutil.rmtree(self.__path_to_folder)
            os.makedirs(self.__path_to_folder)
            chrome_options.add_experimental_option("prefs", {
                "download.default_directory": self.__path_to_folder})
            self.__driver = webdriver.Chrome(options=chrome_options)
            self.__driver.get(self.__url_name)

    # Making the new download directory
    def initiate_new_download_directory(self, template_name):
        """
        **Modifies download directory of browser**

        Changes download location chrome will use when downloading files

        :param template_name: folder name to be used
        """
        self.__folder_name = template_name.replace(" ", "")
        self.__path_to_folder = self.__current_directory + "\\" + self.__folder_name
        if os.path.exists(self.__path_to_folder):
            shutil.rmtree(self.__path_to_folder)
        os.makedirs(self.__path_to_folder)

    # Setters and getters
    def get_full_path(self):
        return self.__full_path

    def get_path_to_folder(self):
        return self.__path_to_folder

    def get_driver(self):
        return self.__driver

    def close_browser(self):
        self.__driver.close()

    def quit_browser(self):
        self.__driver.quit()
