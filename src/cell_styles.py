"""
Cell formatting data for excel processing for final report output
"""

from openpyxl.styles import Font, Alignment, NamedStyle, PatternFill, Side, Border

CENTER_ALIGN = Alignment(horizontal="center", vertical="center", wrap_text=True)
LEFT_JUSTIFY = Alignment(horizontal="left", vertical="center", wrap_text=False)

THICK_BD = Side(border_style='medium', color="000000")
THIN_BD = Side(border_style='thin', color="000000")

BORDER = Border(top=THICK_BD, left=THICK_BD, right=THICK_BD, bottom=THICK_BD)
THICK_BT_BORDER = Border(top=THICK_BD, left=THIN_BD, right=THIN_BD,  bottom=THICK_BD)

THIN_CELL_BORDER = Border(top=THICK_BD, left=THIN_BD, right=THIN_BD, bottom=THICK_BD)
THIN_BORDER = Border(top=THIN_BD, left=THIN_BD, right=THIN_BD, bottom=THIN_BD)

THICK_LEFT_BORDER = Border(top=THICK_BD, left=THICK_BD, right=THIN_BD, bottom=THICK_BD)
THICK_LR_BORDER = Border(top=THICK_BD, left=THICK_BD, right=THICK_BD, bottom=THICK_BD)


LARGE_HEADER_STYLE = NamedStyle(name="large_header")
LARGE_HEADER_STYLE.font = Font(bold=True)
LARGE_HEADER_STYLE.alignment = CENTER_ALIGN
LARGE_HEADER_STYLE.fill = PatternFill("solid", fgColor="92D050")
LARGE_HEADER_STYLE.border = THICK_BT_BORDER

WIDE_HEADER_STYLE = NamedStyle(name="wide_header")
WIDE_HEADER_STYLE.font = Font(bold=True, color="ffffff")
WIDE_HEADER_STYLE.alignment = CENTER_ALIGN
WIDE_HEADER_STYLE.fill = PatternFill("solid", fgColor="a6a6a6")
WIDE_HEADER_STYLE.border = BORDER

CELL_STYLE = NamedStyle(name="cell_style")
CELL_STYLE.alignment = CENTER_ALIGN
CELL_STYLE.border = THIN_CELL_BORDER

LEFT_JUST_STYLE = NamedStyle(name="left_just_style")
LEFT_JUST_STYLE.alignment = LEFT_JUSTIFY
LEFT_JUST_STYLE.border = THIN_BORDER

INVALID_STYLE = NamedStyle(name="invalid_style")
INVALID_STYLE.alignment = CENTER_ALIGN
INVALID_STYLE.font = Font(color="9c0006")
INVALID_STYLE.fill = PatternFill(fgColor="FFC7CE", fill_type="solid")
INVALID_STYLE.border = THIN_CELL_BORDER

GREY_STYLE = NamedStyle(name="grey_style")
GREY_STYLE.alignment = CENTER_ALIGN
GREY_STYLE.border = THIN_CELL_BORDER
GREY_STYLE.fill = PatternFill(fgColor="d9d9d9", fill_type="solid")

ROW_HEADER_STYLE = NamedStyle(name="row_header_style")
ROW_HEADER_STYLE.alignment = LEFT_JUSTIFY
ROW_HEADER_STYLE.border = BORDER
ROW_HEADER_STYLE.font = Font(bold=True)
