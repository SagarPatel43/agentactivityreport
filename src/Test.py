"""
Test class which can execute selenium operations on a chrome instance
"""

import datetime
import os
import shutil
import time
from datetime import timedelta

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import constants


class TestMethods:
    # Broadsoft Report Template Parameters
    __user_id = "CC_Supervisor"
    __password = "1sup_Iristel123"
    __template_name = "Agent Unavailability Report"
    __report_format = "XLS"
    __sampling_period = "Daily"
    __start_time = "12:00am"
    __end_time = "12:00am"
    __call_completion = "60"
    __short_duration = "60"
    __start_date = ""
    __end_date = ""
    # Required components for Selenium
    driver = None
    wait = None
    action = None
    browser_driver = None
    # Folder paths and file components
    folder_path = ""
    folder_name = ""

    def __init__(self, browser):
        """
        initializes test object with the web browser, driver and folder path

        :param browser: driver for browser to be used
        """
        self.browser_driver = browser
        self.driver = browser.get_driver()
        self.folder_path = browser.get_full_path()
        self.folder_name = browser.get_path_to_folder()
        self.wait = WebDriverWait(self.driver, 20)
        self.action = ActionChains(self.driver)

    # Setters and getters
    def get_template_name(self):
        return self.__template_name

    def set_template_name(self, template_name):
        self.__template_name = template_name

    def set_start_date(self, start_date):
        self.__start_date = start_date

    def set_end_date(self, end_date):
        self.__end_date = end_date

    # Logging into broadfone
    def logging_in(self):
        """
        Logs into the broadfone web portal for agents
        """
        self.wait.until(EC.element_to_be_clickable((By.ID, "isc_T")))
        self.driver.find_element_by_id('isc_T').send_keys(self.__user_id)
        self.driver.find_element_by_id('isc_W').send_keys(self.__password)
        self.driver.find_element_by_class_name('buttonTitle').click()

    # Opening the reporting section in a new tab
    def swap_to_reporting(self):
        """
        Clicks on reporting link to access agent reports
        """
        self.wait.until(EC.element_to_be_clickable((By.ID, "isc_LinkItem_5$20j")))
        self.action.move_to_element(self.driver.find_element_by_id("isc_LinkItem_5$20j")).key_down(Keys.CONTROL) \
            .click() \
            .perform()
        self.driver.switch_to.window(self.driver.window_handles[-1])

    # Selecting the template required
    def choose_template(self):
        """
        Changes the agent template on the web page
        """
        self.wait.until(EC.element_to_be_clickable((By.ID, "isc_1W")))
        self.driver.find_element_by_id("isc_1W").click()
        self.driver.find_element_by_xpath("//*[contains(text(),'" + self.__template_name + "')]").click()

    # Selecting the download format
    def choose_format(self):
        """
        Changes the format of the requested document to xls
        """
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(text(),'HTML')]")))
        self.driver.find_element_by_xpath("//div[contains(text(),'HTML')]").click()
        self.driver.find_element_by_xpath("//*[contains(text(),'" + self.__report_format + "')]").click();

    # Inputting the call completion parameter
    def choose_call_completion(self):
        """
         Inputs the call completion parameter on the web form
        """
        if self.__template_name == "Agent Activity Report":
            self.wait.until(EC.element_to_be_clickable((By.NAME, "callCompletion")))
            self.driver.find_element_by_name("callCompletion").click()
            self.driver.find_element_by_name("callCompletion").send_keys("")
            self.driver.find_element_by_name("callCompletion").send_keys(self.__call_completion)

    # Inputting the short duration parameter
    def choose_short_duration(self):
        """
        Inputs the short duration parameter on the web form
        """
        if self.__template_name == "Agent Activity Report":
            self.wait.until(EC.element_to_be_clickable((By.NAME, "shortDuration")))
            self.driver.find_element_by_name("shortDuration").clear()
            self.driver.find_element_by_name("shortDuration").click()
            self.driver.find_element_by_name("shortDuration").send_keys(self.__short_duration)

    # Choose the sampling type
    def choose_sampling_type(self):
        """
         Inputs the sampling type parameter on the web form
        """

        if self.__template_name != "Agent Call Detail Report":

            if self.__sampling_period != "Daily":
                self.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[contains(text(),'Daily')]")))

                self.driver.find_element_by_xpath("//*[contains(text(),'Daily')]").click()

                self.driver.find_element_by_xpath("//*[contains(text(),'" + self.__sampling_period + "')]").click()

    # Inputting the start date parameter
    def change_start_date(self):
        """
        Inputs the start date parameter on the web form
        """
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[contains(@name,'isc_CustomDateItem')]")))

        self.driver.find_element_by_xpath("//*[contains(@name,'isc_CustomDateItem')]").clear()

        self.driver.find_element_by_xpath("//*[contains(@name,'isc_CustomDateItem')]").send_keys("")
        self.driver.find_element_by_xpath("//*[contains(@name,'isc_CustomDateItem')]").send_keys(self.__start_date)

    # Inputting the end date parameter
    def change_end_date(self):
        """
        Inputs the end date parameter on the web form
        """
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[contains(@name,'isc_CustomDateItem')]")))
        self.driver.find_elements(By.XPATH, "//*[contains(@name,'isc_CustomDateItem')]")[1].clear()
        self.driver.find_elements(By.XPATH, "//*[contains(@name,'isc_CustomDateItem')]")[1].send_keys("")
        self.driver.find_elements(By.XPATH, "//*[contains(@name,'isc_CustomDateItem')]")[1].send_keys(self.__end_date)

    # Inputting the start time parameter
    def change_start_time(self):
        """
        Inputs the start time parameter on the web form
        """
        self.wait.until(EC.element_to_be_clickable((By.NAME, "startTime")))
        self.driver.find_element_by_name("startTime").clear()
        self.driver.find_element_by_name("startTime").send_keys("")
        self.driver.find_element_by_name("startTime").send_keys(self.__start_time)

    # Inputting the end date parameter
    def change_end_time(self):
        """
        Inputs the end time parameter on the web form
        """
        self.wait.until(EC.element_to_be_clickable((By.NAME, "endTime")))
        self.driver.find_element_by_name("endTime").clear()
        self.driver.find_element_by_name("endTime").send_keys("")
        self.driver.find_element_by_name("endTime").send_keys(self.__end_time)

    # Click Run Report
    def generate_report(self):
        """
        Generates the form by clicking on the submit button on the web form
        """
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[contains(text(),'Run Report')]")))

        self.action.move_to_element(self.driver.find_element_by_xpath("//*[contains(text(),'Run Report')]"))

        self.driver.find_element_by_xpath("//*[contains(text(),'Run Report')]").click()

    # Reset the UI after file download is over
    def reset_ui_report_generated(self):
        """
        Resets the UI after a report has been generated to prepare for the next report to be downloaded
        """
        self.wait.until(EC.element_to_be_clickable((By.ID, "isc_21")))
        # Sleep(500);
        self.driver.find_element_by_id("isc_21").click()
        self.wait.until(EC.element_to_be_clickable((By.ID, "isc_1G")))
        # Thread.Sleep(500);
        self.driver.find_element(By.ID, "isc_1G").click()

    # Generate all the reports within the specified period
    def generate_all_reports(self):
        """
        Checks requested time reports and downloads all reports from that time period
        """
        start_time = datetime.datetime(int(self.__start_date[6:10]), int(self.__start_date[0:2]),
                                       int(self.__start_date[3:5]))
        end_time = datetime.datetime(int(self.__end_date[6:10]), int(self.__end_date[0: 2]), int(self.__end_date[3:5]))
        temp_start = self.__start_date
        temp_end = self.__end_date
        while start_time.strftime('%m/%d/%Y') != end_time.strftime('%m/%d/%Y'):
            self.change_start_date()
            self.__end_date = (start_time + timedelta(days=1)).strftime('%m/%d/%Y')
            self.__start_date = start_time.strftime('%m/%d/%Y')
            self.change_end_date()
            self.generate_report()
            self.reset_ui_report_generated()
            start_date_format = self.__start_date.replace("/", "")
            file_name = self.__template_name + start_date_format + constants.XLS
            file_path = self.folder_path + ".xls"

            while not os.path.exists(file_path):
                time.sleep(1)

            if not os.path.isfile(file_path):
                raise ValueError("%s isn't a file!" % file_path)

            time.sleep(1)

            print(file_name + " successfully downloaded")
            shutil.move(self.folder_path + ".xls",
                        self.browser_driver.get_path_to_folder() + "//" + file_name)
            time.sleep(2)
            if not self.driver.find_element_by_xpath("//*[contains(text(),'Run Report')]").is_displayed():
                self.driver.find_element_by_id("isc_21").click()

            start_time = start_time + timedelta(days=1)
            self.__start_date = self.__end_date
        self.__start_date = temp_start
        self.__end_date = temp_end
