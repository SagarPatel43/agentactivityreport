.. Agent Activity Report Parser documentation master file, created by
   sphinx-quickstart on Mon Aug 12 11:40:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Agent Activity Report Parser's Documentation!
========================================================

Agent Activity Report Parser will download agent data from broadsoft and process the data into a final document.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

main.py
===================

.. automodule:: main
   :members:

browser.py
===================

.. automodule:: browser
   :members:

.. autoclass:: BrowserDriver
   :members:

cell_styles.py
===================

.. automodule:: cell_styles
   :members:

compute.py
===================

.. automodule:: compute
   :members:

constants.py
===================

.. automodule:: constants
   :members:

dbconnector.py
===================

.. automodule:: dbconnector
   :members:

report_parsing_service.py
==========================

.. automodule:: report_parsing_service
   :members:

Test.py
==========================

.. automodule:: Test
   :members:

.. autoclass:: TestMethods
   :members:

web_downloader.py
==========================

.. automodule:: web_downloader
   :members:

write_total_report.py
==========================

.. automodule:: write_total_report
   :members:
